<?php

use core\DBConnector;
use core\Templater;
use models\UserModel;
use models\ArticleModel;

session_start();

function __autoload($classname)
{
    $file = __DIR__ . DIRECTORY_SEPARATOR  . str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
    if(file_exists($file)) {
        include_once $file;
        // echo $file . "<br>";
    }
    else {
        die("File with same class name: <b>$classname</b> not found");
    }
}

$uri = $_SERVER['REQUEST_URI'];
$uriParts = explode('/', $uri);

unset($uriParts[0]);
$uriParts = array_values($uriParts);

$controller = (isset($uriParts[0]) && is_string($uriParts[0]) && $uriParts[0] !== '') ? $uriParts[0] : 'article';
$controller = ucfirst($controller);

try {
    if (!file_exists(__DIR__ . sprintf('\controllers\%sController.php', $controller))) {
        throw new core\exceptions\ErrorNotFoundException();
    }

    $id = false;
    if(isset($uriParts[1]) && is_numeric($uriParts[1])) {
        $id = $uriParts[1];
        $uriParts[1] = 'one';
    }
    elseif(isset($uriParts[2]) && is_numeric($uriParts[2])) {
        $id = $uriParts[2];
    }

    if($id) {
        $_GET['id'] = $id;
    }

    $request = new core\Request($_GET, $_POST, $_SERVER, $_COOKIE, $_FILES, $_SESSION);


    $action = (isset($uriParts[1]) && is_string($uriParts[1]) && $uriParts[1] !== '') ? $uriParts[1] : 'index';
    $action = ucfirst($action);
    
    if ((bool)strpos($action, '-')) {
        $actionParts = explode('-', $action);
        for ($i = 1; $i < count($actionParts); $i++) {
            $actionParts[$i] = ucfirst($actionParts[$i]);
        }
        $action = implode('', $actionParts);
    }

    $action = sprintf('action%s', $action);


    $controller = sprintf('controllers\%sController', $controller);
    $controller = new $controller($request);
    $controller->$action();
} catch (\Exception $e) {
    $controller = sprintf('controllers\%sController', 'Base');
    $controller = new $controller($request);
    $controller->errorHandler($e->getMessage(), $e->getTraceAsString()); 
}
$controller->render();