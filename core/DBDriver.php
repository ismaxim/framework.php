<?php

namespace core;

class DBDriver
{
    const FETCH_ALL = 'all';
    const FETCH_ONE = 'one';

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function select(string $sql, array $params = [], string $fetch = self::FETCH_ALL)
    {
        $stmt = $this->pdo->prepare($sql);
		$stmt->execute($params);

        switch($fetch){
            case self::FETCH_ALL : return $stmt->fetchAll();
            case self::FETCH_ONE : return $stmt->fetch();
            default : return false;
        }
    }

    public function insert(string $table, array $params)
    {
        $columns = sprintf('(%s)', implode(', ', array_keys($params)));
        $values = sprintf('(:%s)', implode(', :', array_keys($params)));

        $sql = sprintf("INSERT INTO %s %s VALUES %s", $table, $columns, $values);

		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($params);
        
		return $this->pdo->lastInsertId();
    }

    public function delete(string $table, string $where)
	{
        // добавить проверки
        $sql = sprintf('DELETE FROM %s WHERE %s', $table, $where);

        $stmt = $this->pdo->prepare($sql);
		$stmt->execute();

        return $stmt->rowCount();
        // Возвращает количество строк, затронутых последним SQL-запросом
	}

    public function update(string $table, array $params, string $where)
    {
        // $columns = sprintf('%s', implode(', ', array_keys($params)));
        // $values  = sprintf('%s', implode(', ', array_keys($params)));

        foreach($params as $key => $value) {
            unset($params[$key]);
            $params[] = "$key = :$value";
        }
        $setters = sprintf('%s', implode(', ', $params));

        $sql = sprintf('UPDATE %s SET %s WHERE %s', $table, $setters, $where);
        
        $stmt = $this->pdo->prepare($sql);
		$stmt->execute($params);

        return $stmt->rowCount();
    }
}