<?php

namespace core\validation;

use core\exceptions\ValidatorException;

class Validator
{
    const VARTYPE_STRING = 'string';
    const VARTYPE_INTEGER = 'integer';
    const VARTYPE_INT = 'int';

    // размер текста 
    const SUPER_LENGTH = 5000;

    public $clean = [];
    public $errors = [];
    public $success = false;
    private $rules;

    public function execute(array $fields)
    {
        if (!$this->rules) {
            // ошибка
            throw new ValidatorException('Rules for validation not found', 500);
        }

        foreach ($this->rules as $name => $rules) {
            echo $name . "<br>";

            // проверка на обязательное поле
            if (!isset($fields[$name]) && isset($rules['require'])) {
                $this->errors[$name][] = sprintf('Field "%s" not passed!', $name);
            }
            
            // нет обязательного поля
            /* 
                Нужно починить тут! Цикл запускаеться повторно, отсюда начинается хрень...
            */
            if (!isset($fields[$name]) && (!isset($rules['require']) || !$rules['require'])) {
                continue;
            }

            if (isset($rules['not_blank']) && $this->isBlank($fields[$name])) {
                $this->errors[$name][] = sprintf('Field "%s" is not be blank', $name);
            }
            if (isset($rules['type']) && !$this->isTypeMatching($fields[$name], $rules['type'])) {
                $this->errors[$name][] = sprintf('Field "%s" must be a %s type', $name, $rules['type']);
            }
            if (isset($rules['length']) && !$this->isLengthMatch($fields[$name], $rules['length'])) {
                $this->errors[$name][] = sprintf('Field "%s" has an incorrect length.', $name);
            }

            if (empty($this->errors[$name])) {
                if (isset($rules['type']) && $rules['type'] === self::VARTYPE_STRING) {
                    $this->clean[$name] = htmlspecialchars(trim($fields[$name]));
                }
                elseif (isset($rules['type']) && ($rules['type'] === self::VARTYPE_INT || $rules['type'] = self::VARTYPE_INTEGER)) {
                    $this->clean[$name] = (int)$fields[$name];
                }
                else {
                    $this->clean[$name] = $fields[$name];
                }
            }
        }

        if (empty($this->errors)) {
            $this->success = true;
        }
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function isLengthMatch($field, $length) 
    {
        if ($isArray = is_array($length)) { 
            $min = isset($length[0]) ? $length[0] : false;
            $max = isset($length[1]) ? $length[1] : false;
        }
        else {
            $min = false;
            $max = is_numeric($length) ? $length : (int)$length;
        }

        if ($isArray && (!$min || !$max)) {
            // ошибка
            throw new ValidationException('Incorrect data given to method isLengthMatch');
        }
        
        if (!$isArray && !$max) {
            // ошибка
            throw new ValidationException('Incorrect data given to method isLengthMatch');
        }
        
        $minIsMatch = $min ? $this->isLengthMinMatch($field, $min) : false;
        $maxIsMatch = $max ? $this->isLengthMaxMatch($field, $max) : false;

        return $isArray ? $maxIsMatch && $minIsMatch : $maxIsMatch;
    }

    public function isLengthMinMatch($field, $length)
    {
        return mb_strlen($field) >= $length;
    }
    public function isLengthMaxMatch($field, $length)
    {
        return mb_strlen($field) <= $length;
    }

    public function isTypeMatching($field, $type)
    {
        switch ($type) {
            case self::VARTYPE_STRING :
                return is_string($field);
                break;
            case self::VARTYPE_INT :
            case self::VARTYPE_INTEGER :
                return gettype($field) === self::VARTYPE_INTEGER && ctype_digit($field);
                break;
            default :
                // ошибка
                throw new ValidationException('Incorrect data given to method isTypeMatching');
                break;
        }
    }

    public function isBlank($field)
    {
        $field = trim($field);
        return $field === null || $field === '';
    }
}