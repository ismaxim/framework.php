<?php

namespace core;

class DBConnector
{
	private static $config = [
		'db' => 'mysql',
		'host' => 'localhost',
		'dbname' => 'testdb',
		'username' => 'root',
		'password' => ''
	];

	private static $instance;

	public static function getConnect()
	{
		if (self::$instance === null) {
			self::$instance = self::getPDO();
		}

		return self::$instance;
	}

	private static function getPDO()
	{
		$dsn = sprintf('%s:host=%s;dbname=%s', 
			self::$config['db'], 
			self::$config['host'], 
			self::$config['dbname']
		);
		
		return new \PDO(
			$dsn, 
			self::$config['username'], 
			self::$config['password']
		);
	}
}