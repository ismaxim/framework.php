<?php

namespace core\exception;

class ErrorNotFoundException extends \Exception
{
    public function __consturct($massage = 'Page Not Found', $code = 404)
    {
        parent::__construct($massage, $code);
    }
}