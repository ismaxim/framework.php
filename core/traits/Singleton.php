<?php

namespace core\traits;

trait Singleton
{
    protected static $instance;

	public static function instance()
	{
		if (static::$singleton_instance === null) {
			static::$singleton_instance = new static();
		}

		return self::$instance;
    }
    
    protected function __construct(){}
    protected function __clone(){}
    protected function __sleep(){}
    protected function __wakeup(){}
}