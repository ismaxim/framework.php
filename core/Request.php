<?php

namespace core;

class Request
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    private $get;
    private $post;
    private $server;
    private $cookie;
    private $files;
    private $session;

    public function __construct(array $get, array $post, array $server, array $cookie, array $files, array $session)
    {
        $this->get = $get;
        $this->post = $post;
        $this->server = $server;
        $this->cookie = $cookie;
        $this->files = $files;
        $this->session = $session;
    }

    public function get($key = null)
    {
        return $this->getArr($this->get, $key);
    }

    public function post($key = null)
    {
        return $this->getArr($this->post, $key);
    }
    
    public function server($key = null)
    {
        return $this->getArr($this->server, $key);
    }
    
    public function cookie($key = null)
    {
        return $this->getArr($this->cookie, $key);
    }
    
    public function files($key = null)
    {
        return $this->getArr($this->files, $key);
    }

    public function session($key = null)
    {
        return $this->getArr($this->session, $key);
    }

    public function isPost()
    {
        return $this->server['REQUEST_METHOD'] === self::METHOD_POST;
    }

    public function isGet()
    {
        return $this->server['REQUEST_METHOD'] === self::METHOD_GET;
    }

    private function getArr(array $arr, $key = null)
    {   
        if(!isset($key)) {
            return $arr;
        }
        elseif(isset($arr[$key])) {
            return $arr[$key];
        }

        return null;
    }
}