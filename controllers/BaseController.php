<?php

namespace controllers;

use core\Request;
use core\exceptions\ErrorNotFoundException;

class BaseController
{
    protected $title;
    protected $content;
    protected $request;

    public function __construct(Request $request = null)
    {
        $this->request = $request;
        $this->title = 'Testblog';
        $this->content = '';
    }

    public function __call($method, $params)
    {
        throw new ErrorNotFoundException();
    }

    public function render()
    {
        echo $this->build(
                'main.html.php', 
                [
                    'title' => $this->title,
                    'content' => $this->content
                ]
            );
    }

    public function errorHandler(string $massage, string $trace)
    {
        $this->content = $massage . "<br>" . $trace;
    }
    
    protected function redirect(string $uri)
    {
        header(sprintf('Location: %s', $uri));
        exit();
    }

    protected function build(string $template, array $params = [])
    {
        ob_start();
        extract($params);
        include_once __DIR__ . '\..\\views\\' . $template;
        return ob_get_clean();
    }
}