<?php

namespace controllers;

use models\ArticleModel;
use core\DBConnector;
use core\DBDriver;
use core\validation\Validator;
use core\exceptions\ModelException;

class ArticleController extends BaseController
{
    public function actionIndex()
    {
        $this->title .= ' :: список статей';

        $mArticle = new ArticleModel(
            new DBDriver(DBConnector::getConnect()),
            new Validator()
        );
        $articles = $mArticle->getAll();

        $this->content = $this->build(
            'articles.html.php', 
            [
                'articles' => $articles
            ]
        );
    }

    public function actionOne()
    {
        $id = $this->request->get('id');
        
        $mArticle = new ArticleModel(
            new DBDriver(DBConnector::getConnect()),
            new Validator()
        );
        $article = $mArticle->getById($id);

        if (!$article) {
            // 404
        }

        // if ($this->request->isPost()) {
        //     // делаем валидацию
        //     // обрабатываем форму
        // }

        // отображаем форму на экране

        $this->title .= ' :: ' . $article['title'];
        $this->content = $this->build(
            'article.html.php', 
            [
                'title' => $article['title'],
                'content' => $article['content'],
                'pub_date' => $article['pub_date']
            ]
        );
    }

    public function actionAdd()
    {
        $this->title .= ' :: добавление новой статьи';
        if ($this->request->isPost()) {
            $mArticle = new ArticleModel(
                new DBDriver(DBConnector::getConnect()),
                new Validator()
            );
            try {
                $id = $mArticle->add(
                    [
                        'title' => $this->request->post('title'),
                        'content' => $this->request->post('content')
                    ]
                );
                $this->redirect(sprintf('/article/%s', $id));
            } catch (ModelException $e) {
                return $e->getErrors();
                die;
            }

        }
        $this->content = 'Форма ввода полей для статьи';
    }
}