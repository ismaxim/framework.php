<?php

namespace controllers;

use models\UserModel;
use core\User;
use core\DBDriver;
use core\DBConnector;
use core\validation\Validator;
use core\exceptions\ModelException;

class UserController extends BaseController
{
    public function actionSignUp()
    {
        $errors = [];
        $this->title .= ' :: Регистрация';

        if ($this->request->isPost()) {
            $mUser = new UserModel(
                new DBDriver(DBConnector::getConnect()),
                new Validator()
            );
            
            $user = new User($mUser);
            try {
                $user->signUp($this->request->post());
                $this->redirect('/');
            } catch (\Exception $e) {
                $errors = $e->getErrors();
            }
        }

        $this->content = $this->build(
            'sign-up.html.php', 
            [
                'errors' => $errors
            ]
        );
    }

    public function actionSignIn()
    {
        $errors = [];
        $this->title .= " :: Авторизация";

        if ($this->request->isPost()) {
            $mUser = new UserModel(
                new DBDriver(DBConnector::getConnect()),
                new Validator()
            );
            
            $user = new User($mUser);
            try {
                $user->signUp($this->request->post());
                $this->redirect('/');
            } catch (\Exception $e) {
                $errors = $e->getErrors();
            }
        }

        $this->content = $this->build(
            'sign-in.html.php', 
            [
                'errors' => $errors
            ]
        );
    }
}