<?php foreach($articles as $article) : ?>
    <div>
        <h2>
            <a href="/article/<?=$article['id'];?>">
                <?=$article['title'];?>
            </a>
        </h2>
        <p><?=$article['content'];?></p>
    </div>
<?php endforeach; ?>