<?php

namespace models;

use core\DBDriver;
use core\validation\Validator;

class ArticleModel extends BaseModel
{
	protected $schema = [
		'id' => [
			'type' => Validator::VARTYPE_INTEGER
		],

		'title' => [
			'type' => Validator::VARTYPE_STRING,
			'length' => [10, 150], // Диапазон
			'not_blank' => true,
			'require' => true // означает что поле обязательное
		],

		'content' => [
			'type' => Validator::VARTYPE_STRING,
			'length' => Validator::SUPER_LENGTH,
			'not_blank' => true,
			'require' => true
		]
	];

	public function __construct(DBDriver $db, Validator $validator)
	{
		parent::__construct($db, $validator, 'articles');
		$this->validator->setRules($this->schema);
	}

	public function getPostById($id)
	{
		$res = $this->getById($id);
		if ($res) {
			throw new Exception();
		}

		return $res;
	}
}