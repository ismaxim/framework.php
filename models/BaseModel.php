<?php

namespace models;

use core\DBDriver;
use core\validation\Validator;
use core\exceptions\ModelException;

abstract class BaseModel
{
	protected $db;
	protected $table;
	protected $validator;

	public function __construct(DBDriver $db, Validator $validator, string $table)
	{
		$this->db = $db;
		$this->validator = $validator;
        $this->table = $table;
	}

	public function getAll()
	{
		$sql = sprintf('SELECT * FROM %s', $this->table);
		return $this->db->select($sql);
	}	
	
	public function getById($id)
	{
		$sql = sprintf('SELECT * FROM %s WHERE id = :id', $this->table);
		return $this->db->select($sql, ['id' => $id], DBDriver::FETCH_ONE);
	}	

	public function add(array $params, bool $needValidation = true)
	{
		if ($needValidation) {
			$this->validator->execute($params);

			if (!$this->validator->success) {
				// обработать ошибку
				throw new ModelException($this->validator->errors);
				$this->validator->errors;
			}

			$params = $this->validator->clean;
		}

		return $this->db->insert($this->table, $params);
	}
	
	public function delete(array $params, string $where)
	{
		return $this->db->delete($this->table, $where);
	}

	public function edit(array $params, string $where)
	{
		return $this->db->update($this->table, $params, $where);
	}
}