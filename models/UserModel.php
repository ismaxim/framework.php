<?php

namespace models;

use core\DBDriver;
use core\validation\Validator;

class UserModel extends BaseModel
{
	const SOLT = '1$."*@_!**@+_!#$$%@!D';
	
	protected $schema = [
		'id' => [
			'type' => Validator::VARTYPE_INTEGER
		],
		
		'login' => [
			'type' => Validator::VARTYPE_STRING,
			'length' => [3, 50],
			'not_blank' => true,
			'require' => true
		],
		
		'password' => [
			'type' => Validator::VARTYPE_STRING,
			'length' => [8, 50],
			'not_blank' => true,
			'require' => true
		]
	];
		
	public function __construct(DBDriver $db, Validator $validator)
	{
		parent::__construct($db, $validator, 'users');
		$this->validator->setRules($this->schema);
	}
	
	public function signUp(array $fields)
	{
		$this->validator->execute($fields);
		
		if (!$this->validator->success) {
			// throw new exception
		}

		return $this->add(
			[
				'login' => $this->validator->clean['login'],
				'password' => $this->getHash($this->validator->clean['password'])
			]
		);
	}

	public function getHash($password)
	{
		return md5($password . self::SOLT);
	}
}